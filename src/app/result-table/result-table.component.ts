import {Component, OnInit, ViewChild} from '@angular/core';
import {MessageService} from '../shared/message.service';
import {Subscription} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';

export interface ResultElement {
  date: number;
  open: number;
  close: number;
  high: number;
  low: number;
  volume: number;
}

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.css']
})
export class ResultTableComponent implements OnInit {
  displayedColumns: string[] = ['date', 'open', 'high', 'low', 'close', 'volume'];
  dataSource: ResultElement[] = [];
  subscription: Subscription;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.dataSource = [];
      if (message) {
        this.dataSource = message;
      }
    });
  }

}
