import {Component, OnInit, ViewChild} from '@angular/core';
import {ChartDataSets} from 'chart.js';
import {Color, BaseChartDirective, Label} from 'ng2-charts';
import {MessageService} from '../shared/message.service';
import {Subscription} from 'rxjs';
import {ResultElement} from '../result-table/result-table.component';
import moment from 'moment';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
})
export class LineChartComponent implements OnInit {
  subscription: Subscription;

  public lineChartData: ChartDataSets[] = [
    {data: [], label: 'Open'},
    {data: [], label: 'Close'},
  ];
  public lineChartLabels: Label[];

  public lineChartColors: Color[] = [
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  @ViewChild(BaseChartDirective, {static: true}) chart: BaseChartDirective;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        this.lineChartData = [
          {data: this._filter(message, 'open'), label: 'Open'},
          {data: this._filter(message, 'close'), label: 'Close'},
        ];
        this.lineChartLabels = this._filter(message, 'date', true);
      }
    });
  }

  private _filter(resultElementArray: ResultElement[], key: string, isDate = false) {
    return resultElementArray.map((resultElement) => {
      return isDate ? moment(resultElement[key] * 1000).format('YYYY-MM-DD') : resultElement[key];
    }).reverse();
  }
}
