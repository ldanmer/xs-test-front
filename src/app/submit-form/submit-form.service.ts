import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {companySymbol, SubmitForm} from './submit-form';

import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export default class SubmitFormService {

  constructor(private http: HttpClient) {
  }

  getSymbols(): Observable<companySymbol[]> {
    return this.http.get<companySymbol[]>(`${environment.serviceUrl}/symbols`)
      .pipe(
        catchError(this.handleError)
      );
  }

  postData(submitForm: SubmitForm): Observable<any> {
    return this.http.post<SubmitForm>(`${environment.serviceUrl}/submit`, submitForm)
      .pipe(
        catchError(this.handleError)
      );
  }


  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${err.error.message}`;
    } else {
      errorMessage = `Server error: ${JSON.stringify(err.error)}`;
    }
    return throwError(errorMessage);
  }
}
