import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';
import {debounceTime, map, startWith, takeWhile} from 'rxjs/operators';
import {companySymbol} from './submit-form';
import ListValidator from '../shared/list-validator.validator';
import SubmitFormService from './submit-form.service';
import moment from 'moment';
import {MessageService} from '../shared/message.service';
@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.css']
})
export class SubmitFormComponent implements OnInit, OnDestroy {
  submitForm: FormGroup;
  emailError: string;
  symbolError: string;
  startDateError: string;
  endDateError: string;
  componentActive = true;
  maxStartDate = new Date();
  maxEndDate = new Date();
  minStartDate: Date;
  minEndDate = new Date();

  private validationMessages = {
    required: 'This field is required',
    email: 'Please enter a valid email address.',
    forbiddenValue: 'This value is not in the list',
  };

  options: companySymbol[] = [];

  constructor(private fb: FormBuilder, private service: SubmitFormService, private messageService: MessageService) {

  }

  ngOnInit(): void {

    this.submitForm = this.fb.group({
      companySymbol: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      startDate: [new Date(), [Validators.required]],
      endDate: [new Date(), [Validators.required]],
    });

    const emailControl = this.submitForm.get('email');
    emailControl.valueChanges.pipe(
      debounceTime(500),
      takeWhile(() => this.componentActive),
    ).subscribe(
      () => this.emailError = this.setMessage(emailControl)
    );

    const companySymbolControl = this.submitForm.get('companySymbol');
    companySymbolControl.valueChanges.pipe(
      startWith(''),
      takeWhile(() => this.componentActive),
      map(name => name ? this._filter(name) : this.options.slice())
    ).subscribe(
      () => this.symbolError = this.setMessage(companySymbolControl)
    );

    this.service.getSymbols().pipe(
      takeWhile(() => this.componentActive),
    ).subscribe(
      (options) => {
        this.options = options;
        this.submitForm.controls.companySymbol.setValidators([ListValidator(this.options)]);
      }
    );

    const startDateControl = this.submitForm.get('startDate');
    startDateControl.valueChanges.pipe(
      takeWhile(() => this.componentActive),
    ).subscribe(
      (value) => this.minEndDate = value
    );
    const endDateControl = this.submitForm.get('endDate');
    endDateControl.valueChanges.pipe(
      takeWhile(() => this.componentActive),
    ).subscribe(
      (value) => this.maxStartDate = value
    );
  }

  ngOnDestroy(): void {
    this.componentActive = false;
    this.messageService.clearMessages();
  }

  formSubmit() {
    if (this.submitForm.valid) {
      this.service.postData({
        companySymbol: this.submitForm.get('companySymbol').value,
        email: this.submitForm.get('email').value,
        startDate: moment(this.submitForm.get('startDate').value).format('YYYY-MM-DD'),
        endDate: moment(this.submitForm.get('endDate').value).format('YYYY-MM-DD'),
      }).subscribe(
        response => this.messageService.sendMessage(response),
        error => console.error(error));
    }
  }

  setMessage(c: AbstractControl): string | null {
    if ((c.touched || c.dirty) && c.errors) {
      return Object.keys(c.errors).map(
        key => this.validationMessages[key]).join(', ');
    }
    return null;
  }

  private _filter(name: string): companySymbol[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}
