export type companySymbol = string;

export interface SubmitForm {
  companySymbol: companySymbol;
  email: string;
  startDate: string;
  endDate: string;
}
