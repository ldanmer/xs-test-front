import {AbstractControl, ValidatorFn} from '@angular/forms';
import {companySymbol} from '../submit-form/submit-form';

export default function ListValidator(optionsList: companySymbol[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return optionsList.some((value) => value === control.value) ? null : {forbiddenValue: {value: control.value}};
  };
}
