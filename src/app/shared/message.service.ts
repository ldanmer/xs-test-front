import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SubmitForm} from '../submit-form/submit-form';


@Injectable({providedIn: 'root'})
export class MessageService {
  private subject = new Subject<any>();

  sendMessage(submitForm: SubmitForm) {
    this.subject.next(submitForm);
  }

  clearMessages() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
